# springboot-directory-service


 ## Intro
 Directory listing service that allows a client application to obtain the full directory listing of a given path.


 ## Docker
### pull
```
docker pull rolandjurd/directory-service
```

### run
```
docker run -p 8080:8080 rolandjurd/directory-service:latest
```

## API
The directory listing service is exposed on port 8080
```
http://localhost:8080/directory/search?
```

 ## Request
 ### Query Parameters

 #### path
 the listing path
 #### example
 ```
path=/home/test
http://localhost:8080/directory/search?path=/home/test
```
 #### limit(optional)
limit the amount of results that are returned by the listing service. The default is 1000.
 #### example
 ```
limit=100
http://localhost:8080/directory/search?path=/home/test&limit=100
```

 #### offset(optional)
 Point in the list where the listing will begin. The default is 0
 #### example
 ```
offset=11
http://localhost:8080/directory/search?path=/home/test&limit=100&offset=11
```


 ## Response

 #### count
 Number of items returned

 #### offset
 Starting point specified

#### totalCount
Total amount of items in directory 

#### items
Directory items. This could include files or folders, hidden and non hidden.

- path -> the absolute path of the file/folder
- size -> size of file/folder in bytes
- hidden -> hidden file/folder or not
- creationTime -> time file/folder was created
- lastModifiedTime -> time file/folder was last modified
- lastAccessedTime -> time file/folder was last accessed

### Example
```
{
   "count":2,
   "offset":0,
   "totalCount":2,
   "items":[
      {
         "path":"/home/test/testfile.txt",
         "size":"416 bytes",
         "type":"File",
         "hidden":"no",
         "creationTime":"2020-05-30T07:31:28Z",
         "lastModifiedTime":"2022-03-15T18:08:36Z",
         "lastAccessedTime":"2022-03-15T18:10:19Z"
      },
      {
         "path":"/home/test/testFolder",
         "size":"416 bytes",
         "type":"Folder",
         "hidden":"no",
         "creationTime":"2020-03-30T07:31:28Z",
         "lastModifiedTime":"2022-01-15T18:08:36Z",
         "lastAccessedTime":"2022-03-15T18:10:19Z"
      }
   ]
}
```

### Error Response
#### fieldName
The query parameter that errored

#### messageError
Short error message

### Example
```
{"errorMessage":[{"fieldName":"search.limit","messageError":"must be greater than or equal to 0"}]}
```
